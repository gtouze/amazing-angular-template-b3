export const presentation = [
    {
        title: 'Adrien Percevault',
        subtitle: 'Ingé B3 - Développeur',
        card_header: 'assets/animals/aigle.jpg',
        card_thumbnail: 'assets/animals/thumbnail/aigle.jpg',
        content: 'Je suis un développeur en ingé B3.'
    },
    {
        title: 'Anthony Elineau',
        subtitle: 'Ingé B3 - Développeur',
        card_header: 'assets/animals/lion.jpg',
        card_thumbnail: 'assets/animals/thumbnail/lion.jpg',
        content: 'Je suis un développeur en ingé B3.'
    },
    {
        title: 'Anthony Elineau',
        subtitle: 'Ingé B3 - Développeur',
        card_header: 'assets/animals/lion.jpg',
        card_thumbnail: 'assets/animals/thumbnail/lion.jpg',
        content: 'Je suis un développeur en ingé B3.'
    }
];
//# sourceMappingURL=presentation.js.map