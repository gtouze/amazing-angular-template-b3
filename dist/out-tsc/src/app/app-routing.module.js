import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NantesComponent } from './nantes/nantes.component';
import { RennesComponent } from './rennes/rennes.component';
import { AngersComponent } from './angers/angers.component';
import { HomeComponent } from './home/home.component';
import { DetailComponent } from './detail/detail.component';
const routes = [
    { path: 'nantes', component: NantesComponent },
    { path: 'rennes', component: RennesComponent },
    { path: 'angers', component: AngersComponent },
    { path: 'home', component: HomeComponent },
    { path: 'detail/:ville/:nom', component: DetailComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full' }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forRoot(routes)],
        exports: [RouterModule]
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map