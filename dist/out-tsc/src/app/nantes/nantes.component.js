import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
let NantesComponent = class NantesComponent {
    constructor(api, router) {
        this.api = api;
        this.router = router;
        this.subscription = new Subscription();
        this.displayedColumns = ['id', 'ville', 'date', 'nom', 'place'];
    }
    ngOnInit() {
        this.getData();
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    getData() {
        this.subscription = this.api.getDataNantes().subscribe(res => {
            this.dataSource = res;
        });
    }
    getDetail(ville, noms) {
        this.router.navigate([`detail1/${ville}/${noms}`]);
    }
};
tslib_1.__decorate([
    ViewChild(MatPaginator, { static: true }),
    tslib_1.__metadata("design:type", MatPaginator)
], NantesComponent.prototype, "paginator", void 0);
NantesComponent = tslib_1.__decorate([
    Component({
        selector: 'app-nantes',
        templateUrl: './nantes.component.html',
        styleUrls: ['./nantes.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [ApiService, Router])
], NantesComponent);
export { NantesComponent };
//# sourceMappingURL=nantes.component.js.map