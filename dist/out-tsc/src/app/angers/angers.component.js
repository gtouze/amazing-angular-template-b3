import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
let AngersComponent = class AngersComponent {
    constructor(api, router) {
        this.api = api;
        this.router = router;
        this.subscription = new Subscription();
        this.displayedColumns = ['id', 'ville', 'date', 'nom', 'place'];
        this.dataSource = new MatTableDataSource();
    }
    ngOnInit() {
        this.getData();
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    getData() {
        this.subscription = this.api.getDataAngers().subscribe(res => {
            console.log(res);
            this.dataSource = res;
        });
    }
    getDetail(ville, noms) {
        this.router.navigate([`detail1/${ville}/${noms}`]);
    }
};
tslib_1.__decorate([
    ViewChild(MatPaginator, { static: true }),
    tslib_1.__metadata("design:type", MatPaginator)
], AngersComponent.prototype, "paginator", void 0);
AngersComponent = tslib_1.__decorate([
    Component({
        selector: 'app-angers',
        templateUrl: './angers.component.html',
        styleUrls: ['./angers.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [ApiService, Router])
], AngersComponent);
export { AngersComponent };
//# sourceMappingURL=angers.component.js.map