import * as tslib_1 from "tslib";
import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
let ApiService = class ApiService {
    constructor(http) {
        this.http = http;
        this.url = environment.Api.url + ':' + environment.Api.port;
    }
    getDataAll() {
        return this.http.get(this.url + '/all');
    }
    getByTown(ville, nom) {
        return this.http.get(this.url + '/' + ville + '/' + nom);
    }
    getDataNantes() {
        return this.http.get(this.url + '/Nantes');
    }
    getDataRennes() {
        return this.http.get(this.url + '/Rennes');
    }
    getDataAngers() {
        return this.http.get(this.url + '/Angers');
    }
    jsonData(ville, nom) {
        return this.http.get(this.url + '/' + ville + '/' + nom).pipe(map(result => result));
    }
};
ApiService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], ApiService);
export { ApiService };
//# sourceMappingURL=api.service.js.map