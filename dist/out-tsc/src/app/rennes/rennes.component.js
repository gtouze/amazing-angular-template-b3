import * as tslib_1 from "tslib";
import { Router } from '@angular/router';
import { Component, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
let RennesComponent = class RennesComponent {
    constructor(api, router) {
        this.api = api;
        this.router = router;
        this.subscription = new Subscription();
        this.displayedColumns = ['id', 'ville', 'date', 'nom', 'place'];
        this.dataSource = new MatTableDataSource();
    }
    ngOnInit() {
        this.subscription = this.api.getDataRennes().subscribe(res => {
            console.log(res);
            this.dataSource = res;
        });
        this.dataSource.paginator = this.paginator;
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    getDetail(ville, noms) {
        this.router.navigate([`detail1/${ville}/${noms}`]);
    }
};
tslib_1.__decorate([
    ViewChild(MatPaginator, { static: true }),
    tslib_1.__metadata("design:type", MatPaginator)
], RennesComponent.prototype, "paginator", void 0);
RennesComponent = tslib_1.__decorate([
    Component({
        selector: 'app-rennes',
        templateUrl: './rennes.component.html',
        styleUrls: ['./rennes.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [ApiService, Router])
], RennesComponent);
export { RennesComponent };
//# sourceMappingURL=rennes.component.js.map