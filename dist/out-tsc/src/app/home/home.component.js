import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
let HomeComponent = class HomeComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    goToNantes() {
        this.router.navigate(['/nantes']);
    }
    goToAngers() {
        this.router.navigate(['/angers']);
    }
    goToRennes() {
        this.router.navigate(['/rennes']);
    }
};
HomeComponent = tslib_1.__decorate([
    Component({
        selector: 'app-home',
        templateUrl: './home.component.html',
        styleUrls: ['./home.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [Router])
], HomeComponent);
export { HomeComponent };
//# sourceMappingURL=home.component.js.map