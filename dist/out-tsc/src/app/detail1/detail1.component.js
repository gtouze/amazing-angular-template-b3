import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { DatePipe } from '@angular/common';
import { Chart } from 'chart.js';
let Detail1Component = class Detail1Component {
    constructor(route, api, datepipe) {
        this.route = route;
        this.api = api;
        this.datepipe = datepipe;
        this.date = [];
        this.data = [];
        this.place_max = [];
        this.place_dispo = [];
    }
    ngOnInit() {
        this.subs = this.api.getByTown(this.route.snapshot.paramMap.get('ville'), this.route.snapshot.paramMap.get('nom')).subscribe(data => {
            data.forEach(element => {
                this.place_max.push(element.place_max);
                this.place_dispo.push(element.place_dispo);
                this.date.push(this.datepipe.transform(element.date, 'yyyy-MM-dd HH:mm:SS'));
            });
            this.chartLine = new Chart('canvas', {
                type: 'line',
                data: {
                    labels: this.date,
                    datasets: [
                        {
                            label: "place disponible",
                            data: this.place_dispo,
                            borderColor: '#3cba9f'
                        }
                    ]
                },
                options: {
                    legend: {
                        display: true
                    },
                    scales: {
                        xAxes: [{
                                display: true
                            }],
                        yAxes: [{
                                display: true
                            }]
                    }
                }
            });
        });
        console.log("je suis les places dispo :", this.place_dispo);
        console.log("je suis la date", this.date);
    }
};
Detail1Component = tslib_1.__decorate([
    Component({
        selector: 'app-detail1',
        templateUrl: './detail1.component.html',
        styleUrls: ['./detail1.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute,
        ApiService,
        DatePipe])
], Detail1Component);
export { Detail1Component };
//# sourceMappingURL=detail1.component.js.map