import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NantesComponent } from './nantes/nantes.component';
import { RennesComponent } from './rennes/rennes.component';
import { AngersComponent } from './angers/angers.component';
import { HomeComponent } from './home/home.component';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [
   { path: 'nantes', component: NantesComponent},
   { path: 'rennes', component: RennesComponent},
   { path: 'angers', component: AngersComponent},
   { path: 'home', component: HomeComponent},
   { path: 'detail/:ville/:nom', component: DetailComponent},
   { path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
