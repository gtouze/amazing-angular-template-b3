export  interface Ville {
  id?: string;
  ville: string;
  date: Date;
  nom: string;
  place_dispo: number;
  place_max: number;
}
