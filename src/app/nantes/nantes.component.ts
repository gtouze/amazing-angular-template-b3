import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { Subscription } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Ville } from '../interfaces/ville.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nantes',
  templateUrl: './nantes.component.html',
  styleUrls: ['./nantes.component.scss']
})
export class NantesComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();
  displayedColumns: string[] = ['id', 'ville', 'date', 'nom', 'place'];
  dataSource :any;
  constructor(private api: ApiService, private router: Router) {

  }

  ngOnInit() {
   this.getData();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getData() {
    this.subscription = this.api.getDataNantes().subscribe(res => {
      this.dataSource = res;
    });
  }
  getDetail(ville: string, noms:string) {
  
    this.router.navigate([`detail/${ville}/${noms}`]);
  }
}
