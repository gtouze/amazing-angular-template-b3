import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NantesComponent } from './nantes.component';

describe('NantesComponent', () => {
  let component: NantesComponent;
  let fixture: ComponentFixture<NantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
