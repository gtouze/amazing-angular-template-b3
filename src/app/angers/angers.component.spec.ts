import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngersComponent } from './angers.component';

describe('AngersComponent', () => {
  let component: AngersComponent;
  let fixture: ComponentFixture<AngersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
