import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { Subscription } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Ville } from '../interfaces/ville.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-angers',
  templateUrl: './angers.component.html',
  styleUrls: ['./angers.component.scss']
})
export class AngersComponent implements OnInit, OnDestroy {


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  private subscription = new Subscription();
  displayedColumns: string[] = ['id', 'ville', 'date', 'nom', 'place'];
  dataSource : any;
  constructor(private api: ApiService, private router: Router) {

  }

  ngOnInit() {
   this.getData();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getData() {
    this.subscription = this.api.getDataAngers().subscribe(res => {
      this.dataSource = res;
    });
  }
  getDetail(ville: string, noms:string) {
  
    this.router.navigate([`detail/${ville}/${noms}`]);
  }

}
