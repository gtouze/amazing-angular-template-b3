import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  private url = environment.Api.url + ':' + environment.Api.port;


  constructor(private http: HttpClient ) {

   }

   getDataAll(): Observable<any> {
    return this.http.get<any>( this.url + '/all');
   }

   getByTown(ville: string, nom: string): Observable<any> {

     return this.http.get<any>(this.url + '/' + ville + '/' + nom);
   }

   getDataNantes(): Observable<any> {
    return this.http.get<any>( this.url + '/Nantes');
   }

   getDataRennes(): Observable<any> {
    return this.http.get<any>( this.url + '/Rennes');
   }
   getDataAngers(): Observable<any> {
    return this.http.get<any>( this.url + '/Angers');
   }

   jsonData(ville: string, nom: string) {
     return this.http.get(this.url + '/' + ville + '/' + nom).pipe(
      map(result=>
        result)
     )
      
   }
}
