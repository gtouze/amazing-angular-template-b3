import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { Subscription } from 'rxjs';
import { Ville } from '../interfaces/ville.interface';
import { DatePipe } from '@angular/common'

import { Chart } from 'chart.js'

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  subs: Subscription;
  chartLine: any;
  date: any[] = [];
  data: [] = [];
  place_max: any[] = [];
  place_dispo: any[] = [];

  public villes: Ville;


  constructor(private route: ActivatedRoute,
    private api: ApiService,
    private datepipe: DatePipe) { }

  ngOnInit() {

    this.subs = this.api.getByTown(this.route.snapshot.paramMap.get('ville'), this.route.snapshot.paramMap.get('nom')).subscribe(
      data => {
        data.forEach(element => {
          this.place_max.push(element.place_max)
          this.place_dispo.push(element.place_dispo)
          this.date.push(this.datepipe.transform(element.date,'yyyy-MM-dd HH:mm:SS'))
        });
        this.chartLine = new Chart('canvas', {
          type: 'line',
          data: {
            labels: this.date,
            datasets: [
              {
                label: "place disponible",
                data: this.place_dispo,
                borderColor: '#3cba9f'
              }
            ]
          },
          options: {
            legend: {
              display: true
            },
            scales: {
              xAxes: [{
                display: true
              }],
              yAxes: [{
                display: true
              }]
            }
          }
        })
      }
    )
  }
}
