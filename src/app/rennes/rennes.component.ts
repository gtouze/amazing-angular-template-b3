import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Ville } from '../interfaces/ville.interface';


@Component({
  selector: 'app-rennes',
  templateUrl: './rennes.component.html',
  styleUrls: ['./rennes.component.scss']
})

export class RennesComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();
  displayedColumns: string[] = ['id', 'ville', 'date', 'nom', 'place'];
  dataSource: any;
  constructor(private api: ApiService, private router: Router) {

  }

  ngOnInit() {
    this.subscription = this.api.getDataRennes().subscribe(res => {
      this.dataSource = res;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  getDetail(ville: string, noms:string) {
  
    this.router.navigate([`detail/${ville}/${noms}`]);
  }
}
